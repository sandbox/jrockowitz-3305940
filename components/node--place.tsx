import { DrupalNode } from "next-drupal";
import { FormattedText } from "components/formatted-text";
import { DrupalEntity } from "components/entity";

interface NodePlaceProps {
  node: DrupalNode;
}

export function NodePlace({ node, ...props }: NodePlaceProps) {
  return (
    <article {...props}>
      <h1 className="mb-4 text-6xl">{node.title}</h1>

      <section>
        <h2 className="mb-2 text-4xl">General</h2>

        {node.description?.processed && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Description</h3>
            <FormattedText processed={node.description.processed} />
          </div>
        )}

        {node.image && (
          <div className="mb-4">
            <DrupalEntity entity={node.image} />
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Business</h2>

        {node.opening_hours_specification && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Opening hours</h3>
            {/* office_hours */}
            <pre>
              {JSON.stringify(node.opening_hours_specification, null, 2)}
            </pre>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Contact</h2>

        {node.address && (
          <div className="mb-4">
            {/* address */}
            <pre>{JSON.stringify(node.address, null, 2)}</pre>
          </div>
        )}

        {node.geo && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Geolocation</h3>
            {/* geolocation */}
            <pre>{JSON.stringify(node.geo, null, 2)}</pre>
          </div>
        )}

        {node.telephone && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Telephone</h3>
            <a
              className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
              href={"tel:" + node.telephone}
            >
              {node.telephone}
            </a>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Relationships</h2>

        {node.subject_of && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Subject of</h3>
            <div>
              {node.subject_of.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Editorial information</h2>

        {node.field_editorial && (
          <div className="mb-4">
            {/* entity_reference_revisions */}
            <pre>{JSON.stringify(node.field_editorial, null, 2)}</pre>
          </div>
        )}
      </section>
    </article>
  );
}
