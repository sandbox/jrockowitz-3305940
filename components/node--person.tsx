import { DrupalNode } from "next-drupal";
import { FormattedText } from "components/formatted-text";
import { DrupalEntity } from "components/entity";

interface NodePersonProps {
  node: DrupalNode;
}

export function NodePerson({ node, ...props }: NodePersonProps) {
  return (
    <article {...props}>
      <h1 className="mb-4 text-6xl">{node.title}</h1>

      <section>
        <h2 className="mb-2 text-4xl">General</h2>

        {node.description?.processed && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Description</h3>
            <FormattedText processed={node.description.processed} />
          </div>
        )}

        {node.additional_name && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Middle name</h3>
            <div>{node.additional_name}</div>
          </div>
        )}

        {node.family_name && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Last name</h3>
            <div>{node.family_name}</div>
          </div>
        )}

        {node.given_name && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">First name</h3>
            <div>{node.given_name}</div>
          </div>
        )}

        {node.image && (
          <div className="mb-4">
            <DrupalEntity entity={node.image} />
          </div>
        )}

        {node.knows_language && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Knows languages</h3>
            <div>
              {node.knows_language.map((value, i) => (
                <div key={i}>{value}</div>
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Contact</h2>

        {node.email && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Email</h3>
            <a
              className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
              href={"mailto:" + node.email}
            >
              {node.email}
            </a>
          </div>
        )}

        {node.telephone && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Telephone</h3>
            <a
              className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
              href={"tel:" + node.telephone}
            >
              {node.telephone}
            </a>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Links</h2>

        {node.same_as && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Same as</h3>
            <div>
              {node.same_as.map((item, i) => (
                <div key={i}>
                  <a
                    className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                    href={item.uri}
                  >
                    {item.title || item.uri}
                  </a>
                </div>
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Relationships</h2>

        {node.member_of && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Member of</h3>
            <div>
              {node.member_of.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}

        {node.subject_of && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Subject of</h3>
            <div>
              {node.subject_of.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}

        {node.works_for && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Works for</h3>
            <div>
              {node.works_for.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Editorial information</h2>

        {node.field_editorial && (
          <div className="mb-4">
            {/* entity_reference_revisions */}
            <pre>{JSON.stringify(node.field_editorial, null, 2)}</pre>
          </div>
        )}
      </section>
    </article>
  );
}
