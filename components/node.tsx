import * as React from "react";
import { DrupalNode } from "next-drupal";

import { NodeArticle } from "components/node--article";
import { NodeEvent } from "components/node--event";
import { NodeOrganization } from "components/node--organization";
import { NodePage } from "components/node--page";
import { NodePerson } from "components/node--person";
import { NodePlace } from "components/node--place";
import { DrupalEntity } from "components/entity";

export const RESOURCE_TYPES = [
  "node--article",
  "node--event",
  "node--organization",
  "node--page",
  "node--person",
  "node--place",
];

export const RESOURCE_INCLUDES = {
  "node--article":
    "node_type,uid,about,about.node_type,about.uid,about.about,about.image,about.subject_of,about.parent_organization,about.sub_organization,about.primary_image_of_page,about.main_entity,about.member_of,about.works_for,image,image.uid,image.thumbnail,image.image,subject_of,subject_of.node_type,subject_of.uid,subject_of.about,subject_of.image,subject_of.subject_of,subject_of.primary_image_of_page,subject_of.main_entity",
  "node--event":
    "node_type,uid,about,about.node_type,about.uid,about.about,about.image,about.subject_of,about.parent_organization,about.sub_organization,about.primary_image_of_page,about.main_entity,about.member_of,about.works_for,subject_of,subject_of.node_type,subject_of.uid,subject_of.about,subject_of.image,subject_of.subject_of,subject_of.primary_image_of_page,subject_of.main_entity",
  "node--organization":
    "node_type,uid,image,image.uid,image.thumbnail,image.image,parent_organization,parent_organization.node_type,parent_organization.uid,parent_organization.image,parent_organization.parent_organization,parent_organization.sub_organization,parent_organization.subject_of,sub_organization,sub_organization.node_type,sub_organization.uid,sub_organization.image,sub_organization.parent_organization,sub_organization.sub_organization,sub_organization.subject_of,subject_of,subject_of.node_type,subject_of.uid,subject_of.about,subject_of.image,subject_of.subject_of,subject_of.primary_image_of_page,subject_of.main_entity",
  "node--page":
    "node_type,uid,about,about.node_type,about.uid,about.about,about.image,about.subject_of,about.parent_organization,about.sub_organization,about.primary_image_of_page,about.main_entity,about.member_of,about.works_for,primary_image_of_page,primary_image_of_page.uid,primary_image_of_page.thumbnail,primary_image_of_page.image,main_entity,main_entity.image,main_entity.item_list_element,main_entity.main_entity_of_page,main_entity.has_part,subject_of,subject_of.node_type,subject_of.uid,subject_of.about,subject_of.image,subject_of.subject_of,subject_of.primary_image_of_page,subject_of.main_entity",
  "node--person":
    "node_type,uid,image,image.uid,image.thumbnail,image.image,member_of,member_of.node_type,member_of.uid,member_of.image,member_of.parent_organization,member_of.sub_organization,member_of.subject_of,subject_of,subject_of.node_type,subject_of.uid,subject_of.about,subject_of.image,subject_of.subject_of,subject_of.primary_image_of_page,subject_of.main_entity,works_for",
  "node--place":
    "node_type,uid,image,image.uid,image.thumbnail,image.image,subject_of,subject_of.node_type,subject_of.uid,subject_of.about,subject_of.image,subject_of.subject_of,subject_of.primary_image_of_page,subject_of.main_entity",
};

interface NodePageProps {
  resource: DrupalNode;
}

export function Node({ resource }: NodePageProps) {
  switch (resource.type) {
    case "node--article":
      return <NodeArticle node={resource} />;

    case "node--event":
      return <NodeEvent node={resource} />;

    case "node--organization":
      return <NodeOrganization node={resource} />;

    case "node--page":
      return <NodePage node={resource} />;

    case "node--person":
      return <NodePerson node={resource} />;

    case "node--place":
      return <NodePlace node={resource} />;

    default:
      return <DrupalEntity entity={resource} />;
  }
}
