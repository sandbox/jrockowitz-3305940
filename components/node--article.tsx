import { DrupalNode } from "next-drupal";
import { FormattedText } from "components/formatted-text";
import { DrupalEntity } from "components/entity";

interface NodeArticleProps {
  node: DrupalNode;
}

export function NodeArticle({ node, ...props }: NodeArticleProps) {
  return (
    <article {...props}>
      <h1 className="mb-4 text-6xl">{node.title}</h1>

      <section>
        <h2 className="mb-2 text-4xl">General</h2>

        {node.article_body?.processed && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Article body</h3>
            <FormattedText processed={node.article_body.processed} />
          </div>
        )}

        {node.subtype && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Subtype</h3>
            <div>{node.subtype}</div>
          </div>
        )}

        {node.image && (
          <div className="mb-4">
            <DrupalEntity entity={node.image} />
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Relationships</h2>

        {node.about && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">About</h3>
            <div>
              {node.about.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}

        {node.subject_of && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Subject of</h3>
            <div>
              {node.subject_of.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Editorial information</h2>

        {node.field_editorial && (
          <div className="mb-4">
            {/* entity_reference_revisions */}
            <pre>{JSON.stringify(node.field_editorial, null, 2)}</pre>
          </div>
        )}
      </section>
    </article>
  );
}
