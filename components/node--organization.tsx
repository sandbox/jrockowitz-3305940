import { DrupalNode } from "next-drupal";
import { FormattedText } from "components/formatted-text";
import { DrupalEntity } from "components/entity";

interface NodeOrganizationProps {
  node: DrupalNode;
}

export function NodeOrganization({ node, ...props }: NodeOrganizationProps) {
  return (
    <article {...props}>
      <h1 className="mb-4 text-6xl">{node.title}</h1>

      <section>
        <h2 className="mb-2 text-4xl">General</h2>

        {node.description?.processed && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Description</h3>
            <FormattedText processed={node.description.processed} />
          </div>
        )}

        {node.image && (
          <div className="mb-4">
            <DrupalEntity entity={node.image} />
          </div>
        )}

        {node.subtype && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Subtype</h3>
            <div>{node.subtype}</div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Links</h2>

        {node.same_as && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Same as</h3>
            <div>
              {node.same_as.map((item, i) => (
                <div key={i}>
                  <a
                    className="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                    href={item.uri}
                  >
                    {item.title || item.uri}
                  </a>
                </div>
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Relationships</h2>

        {node.parent_organization && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Parent organization</h3>
            <div>
              {node.parent_organization.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}

        {node.sub_organization && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Sub organization</h3>
            <div>
              {node.sub_organization.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}

        {node.subject_of && (
          <div className="mb-4">
            <h3 className="mb-1 text-2xl">Subject of</h3>
            <div>
              {node.subject_of.map((item, i) => (
                <DrupalEntity key={i} entity={item} />
              ))}
            </div>
          </div>
        )}
      </section>

      <section>
        <h2 className="mb-2 text-4xl">Editorial information</h2>

        {node.field_editorial && (
          <div className="mb-4">
            {/* entity_reference_revisions */}
            <pre>{JSON.stringify(node.field_editorial, null, 2)}</pre>
          </div>
        )}
      </section>
    </article>
  );
}
